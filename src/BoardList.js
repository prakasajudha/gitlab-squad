import Axios from 'axios';
import React from 'react';
import { Link, Route } from 'react-router-dom';
import './BoardList.css';
import Project from './Project';
import { TOKEN_URL, BASE_URL } from './Constant';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Card, Button, Row, Col } from 'react-bootstrap';

class BoardList extends React.Component {
    state = {
        errorMessage: "",
        projects: [],
        statistics: []
    };

    async componentDidMount() {
        try {
            const { data: projects } = await Axios.get(`${BASE_URL}/api/v4/projects${TOKEN_URL}`);
            const { data: statistics } = await Axios.get(`${BASE_URL}/api/v4/application/statistics/${TOKEN_URL}`)
            this.setState({ projects, statistics });
        } catch (error) {
            this.setState({ errorMessage: error.message });
        }
    }

    render() {
        return (
            <div className="board-list-container">
                <div className="header">
                    <h1>GitLab Dashboard Data</h1>
                </div>
                
                <div className="ui four statistics">
                    <div className="ui red statistic">
                        <div className="value">{this.state.statistics.projects}</div>
                        <div className="label">Projects</div>
                    </div>
                    <div className="ui yellow statistic">
                        <div className="value">{this.state.statistics.users}</div>
                        <div className="label">Users</div>
                    </div>
                    <div className="ui green statistic">
                        <div className="value">{this.state.statistics.groups}</div>
                        <div className="label">Groups</div>
                    </div>
                    <div className="ui blue statistic">
                        <div className="value">{this.state.statistics.issues}</div>
                        <div className="label">Issues</div>
                    </div>
                </div>
                <div className="card-container">
                    {
                        this.state.projects.map((project) =>
                            project.visibility === 'public' ?
                                <Card className="text-center card-detail">
                                    <Card.Header>Project ID: {`${project.id}`}</Card.Header>
                                    <Card.Body >
                                        <Card.Title>{`${project.name}`}</Card.Title>
                                        <Card.Text>
                                            {`${project.description}`}
                                        </Card.Text>
                                        <Link to={`/project/${project.id}`} className="board-list">
                                            <Button className="card-button" variant="primary">See Project Detail</Button>
                                        </Link>
                                    </Card.Body>
                                </Card>
                                :
                                null
                        )
                    }
                </div>
                <Route exact path={`/project/:boardId`} component={Project} />
            </div >
        );
    }
}

export default BoardList;