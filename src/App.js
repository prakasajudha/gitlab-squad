import './App.css';
import 'semantic-ui-css/semantic.min.css';
import React from 'react';
import Project from './Project';
import BoardList from './BoardList';
import { Route, Switch } from "react-router-dom";


class App extends React.Component {
  state = {
    redirect: false,
    url: '',
    projects: [],
    members: []
  }

  render() {

    return (
      <div className="App">
        <Switch>
          <Route exact path={`/project/:boardId`} component={Project} />
          <Route exact path={`/`} component={BoardList} />
        </Switch>
      </div>
    );
  }
}

export default App;
