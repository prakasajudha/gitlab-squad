import './Project.css'
import Axios from 'axios';
import React from 'react';
import { Table } from 'react-bootstrap';
import { TOKEN_URL, BASE_URL } from './Constant';

class Project extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            projects: [],
            members: [],
            owner: []
        };
    }

    async componentDidMount() {
        try {
            const { data: projects } = await Axios.get(`${BASE_URL}/api/v4/projects/${this.props.match.params.boardId}${TOKEN_URL}`);
            const { data: members } = await Axios.get(`${BASE_URL}/api/v4/projects/${this.props.match.params.boardId}/members${TOKEN_URL}`);
            console.log(projects);
            console.log(members);
            this.setState({ projects, members });
            this.setState({ owner: this.state.projects.owner })
        } catch (error) {
            this.setState({ errorMessage: error.message });
        }
    }

    render() {
        return (
            <div className='board'>
                <div className="header">
                    <h1>{this.state.projects.name}</h1>
                    <h4>Project ID : {this.state.projects.id}</h4>
                </div>

                <div className="table-container">
                    <div className="project-detail-table">
                        <Table striped bordered hover variant="light">
                            <tbody>
                                <tr>
                                    <td>Owner</td>
                                    <td>{this.state.owner.name}</td>
                                </tr>
                                <tr>
                                    <td>Project Name</td>
                                    <td>{this.state.projects.name}</td>
                                </tr>
                                <tr>
                                    <td>Project Description</td>
                                    <td>{this.state.projects.description}</td>
                                </tr>
                                <tr>
                                    <td>Created At</td>
                                    <td>{this.state.projects.created_at}</td>
                                </tr>
                                <tr>
                                    <td>Star Count</td>
                                    <td>{this.state.projects.star_count}</td>
                                </tr>
                                <tr>
                                    <td>Last Activity</td>
                                    <td>{this.state.projects.last_activity_at}</td>
                                </tr>
                                <tr>
                                    <td>Members</td>
                                    <td>
                                        <ul>
                                            {
                                                this.state.members.map((member) =>
                                                    <li>{member.name}</li>
                                                )}
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </Table>
                    </div>
                </div>

                {/* <table class="projectList">
                        <tr>
                            <th>Owner</th>
                            <th>Project Name</th>
                            <th>Project Description</th>
                            <th>Created At</th>
                            <th>Star Count</th>
                            <th>Last Activity</th>
                            <th>Members</th>
                        </tr>
                        <tr>
                            <td>{this.state.owner.name}</td>
                            <td>{this.state.projects.name}</td>
                            <td>{this.state.projects.description}</td>
                            <td>{this.state.projects.created_at}</td>
                            <center>
                                <td>{this.state.projects.star_count} </td>
                            </center>
                            <td>{this.state.projects.last_activity_at}</td>
                            <td>
                                <ul>
                                    {
                                    this.state.members.map((member) => 
                                    <li>{member.name}</li>
                                    )}
                                </ul>
                            </td>
                        </tr>
                    </table> */}
            </div>
        );
    }
}

export default Project;
